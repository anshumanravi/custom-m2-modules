<?php 
/**
 * Custom module two
 * Copyright (C) 2016  Anshuman
 * 
 * This file is part of Sapient/Customtwo.
 * 
 * Sapient/Customtwo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Sapient\Customtwo\Controller\Hello;
 
 
class World extends \Magento\Framework\App\Action\Action {

	protected $resultPageFactory;

	
	public function __construct(
		\Magento\Framework\App\Action\Context $context,
		\Magento\Framework\View\Result\PageFactory $resultPageFactory
	){
		$this->resultPageFactory = $resultPageFactory;
		parent::__construct($context);
	}

	
	public function execute(){
		return $this->resultPageFactory->create();
	}
}
