

Welcome To TCL Custom Magento Module Repository
----------------------------------------------


----------

**Introduction**

TCL project uses two repositories:


 1. Main Project Repo

   This repository hosts Magento EE default code. there should not be any code edit commit in this repo, only commit 
   in will be for Magento version upgrade. 
   https://del.tools.publicis.sapient.com/bitbucket/projects/TABS/repos/m2ee-core/browse


 2. Custom Module Repository & git workflow

  This repo contains all custom modules developed by Team for TCL/Alcatel Project. We will use composer to organize and deploy custom modules.


----------


**Project Setup Steps**

1. Clone Main Project Repo 
 git clone ssh://git@del.tools.publicis.sapient.com/tabs/m2ee-core.git

2. Make sure you are on develop branch

 

    cd m2ee-core  
     git checkout develop

3. Run ' `composer install` '

4. Follow standard Magento installation


----------


**Development Guidelines :** 


Inclusion of new Module  'Xyz' or update of existing module : 

  -- CD to <Magento Root Dir>/vendor/sapient/module-custom/
  -- git branch (And make sure you are on'develop' branch)
  -- git checkout -b <Feature Branch Name>
  -- CD to <Magento Root Dir>/vendor/sapient/module-custom/src/Sapient

------------- START: Steps only for New module ------------------------------------

  -- Create folder 'Xyz' 
  -- Add you module code inside Xyz
  -- create and edit Xyz/composer.json and update it's content following Sample module (Customone)
  -- Cd to <Magento Root Dir>/vendor/sapient/module-custom/
  -- open composer.json
  -- Update 'psr-4' & autoload.files  key with your namespace. follow sample module Customone 


------------- END:   Steps only for New module -------------------------------------

    -- git status
    -- git add <your modified file only>
    -- git commit -m "<Your Message>"
    -- git push origin <Feature Branch Name>
    -- Go to Bitbucket and create PR <Feature Branch Name >  -> develop




2.2 Bringing your module/code change in Magento env

    -- cd to <Magento Root Dir>
    -- edit composer.json
    -- update version or branch of module pointing to your branch
      e.g "sapient/custom-modules": "dev-<Feature BRanch Name>"
    -- Run composer update









